import json
import sys
from text_classificator import classify_text
from numpy import append
import spacy
from spacy_wordnet.wordnet_annotator import WordnetAnnotator
from nltk.corpus import stopwords
from wordfreq import zipf_frequency
from prettytable import PrettyTable
from fitness_context import Word2Vec
import huffman
import math
import random
import qrandom
import copy
import math

nlp = spacy.load("en_core_web_lg")
nlp.add_pipe("spacy_wordnet", after='tagger', config={'lang': nlp.lang})

try:
    with open("./synonyms.json", 'r') as json_file:
        synonyms_table = json.load(json_file)
except FileNotFoundError:
    print("synonyms json not found, please generate 'synonyms.json' first. Use build()")

word2vec = Word2Vec()


def sentence_structure(sentence):
    doc = nlp(sentence)
    print("\nSentence structure :")
    for token in doc:
        print(str(token), "=>", token.pos_, token.tag_)
    print("")

def get_wordnet_synonym(word):
    token = nlp(str(word))[0]
    synsets = token._.wordnet.synsets()
    lemmas_for_synset = []
    max_freq = 0
    max_word = ""
    for s in synsets :
        for lemma in s.lemma_names():
            lemmas_for_synset.append(lemma.lower())
            freq = zipf_frequency(str(lemma.lower()), 'en', wordlist='large')
            if freq>max_freq:
                max_freq = freq
                max_word = lemma.lower()
                #print(max_word)
    if max_word == str(token):
        return set(lemmas_for_synset)
    else:
        get_wordnet_synonym(max_word)

def get_synonym(sentence, target_word):
    try:
        classify_result = classify_text(sentence)
    except RuntimeError:
        #print("sentence is too long")
        raise KeyError
    
    category = classify_result["label"]

    try:
        word = str(target_word).lower()
        data = synonyms_table[word]
    except KeyError:
        #print(f"cannot find word {word} from synonyms table")
        raise KeyError

    if category in data["category"]:
        index = data["category"].index(category)
    else:
        pass
    values =  data["pred_score"]
    index = values.index(max(values))

    synonyms = data["synonym"][0]
    return synonyms
    

def generate_fitness(sentence, token, print_value=False):
    fitness_matrix = word2vec.context_fitness(sentence, token, print_value=print_value)
    window_size = 3
    sentence_array = sentence.split()
    while not validate_context_fitness(fitness_matrix) and window_size<len(sentence_array):
        window_size+=1
        fitness_matrix = word2vec.context_fitness(sentence, token, window_size=window_size, print_value=print_value)
    max_fitness_word = ""
    max_fitness_value = 0
    for word, fitness_value in fitness_matrix.items():
        if fitness_value>max_fitness_value:
            max_fitness_value = fitness_value
            max_fitness_word = word
        
    return fitness_matrix, max_fitness_word
    
def validate_context_fitness(context_fitness):
    # if context fitness contain same value for all words. then this context fitness is invalid
    
    test_val = list(context_fitness.values())[0]
    for element in context_fitness:
        if context_fitness[element] != test_val:
            return True
    return False

def position_tagging(sentence : str, print_value=False):
    if print_value:
        sentence_structure(sentence=sentence)
    doc = nlp(sentence)

    verb = {"position":None,"token":None,"synonym":[]}
    adjective = {"position":None,"token":None,"synonym":[]}
    noun = {"position":None,"token":None,"synonym":[]}

    word_count = 0
    noun_found = False
    # because there is a possibility that sentence has more than one Verb, Adjective or Noun, we only pick first token that appear on the sentence
    for token in doc:
        if token.pos_=='VERB' and verb["position"] is None:
            fitness_matrix, max_word = generate_fitness(sentence, token)

            if not validate_context_fitness(fitness_matrix):
                print(f"failed get context fitness for VERB = {str(token)}")
                continue
            verb["position"] = word_count
            verb["token"] = token
            verb["synonym"] = word2vec.get_synonym(token)
            verb["fitness_matrix"] = fitness_matrix
            verb["max_fitness_word"] = max_word

        elif token.pos_=='ADJ' and adjective["position"] is None:
            fitness_matrix, max_word = generate_fitness(sentence, token)

            if not validate_context_fitness(fitness_matrix):
                print(f"failed get context fitness for ADJECTIVE = {str(token)}")
                continue
            adjective["position"] = word_count
            adjective["token"] = token
            adjective["synonym"] = word2vec.get_synonym(token)
            adjective["fitness_matrix"] = fitness_matrix
            adjective["max_fitness_word"] = max_word

        elif token.pos_=='NOUN' and not noun_found:
            fitness_matrix, max_word = generate_fitness(sentence, token)

            if not validate_context_fitness(fitness_matrix):
                print(f"failed get context fitness for NOUN = {str(token)}")
                continue
            noun["position"] = word_count
            noun["token"] = token
            noun["synonym"] = word2vec.get_synonym(token)
            noun["fitness_matrix"] = fitness_matrix
            noun["max_fitness_word"] = max_word
            noun_found = True
            
        elif token.tag_ == 'NNP' and not noun_found:
            try:
                synonyms = word2vec.get_synonym(token)
            except KeyError:
                word_count+=1
                continue
            fitness_matrix, max_word = generate_fitness(sentence, token)

            if not validate_context_fitness(fitness_matrix):
                print(f"failed get context fitness for NOUN = {str(token)}")
                continue
            
            noun["synonym"] = synonyms
            noun["position"] = word_count
            noun["token"] = token
            noun["fitness_matrix"] = fitness_matrix
            noun["max_fitness_word"] = max_word

        word_count+=1

    if print_value:
        if verb["token"]:
            print("found VERB that can be replaced : ", str(verb["token"]))
            generate_fitness(sentence, verb["token"], print_value=print_value)
            print("context fitness : ", verb["fitness_matrix"])
            print("")
        if adjective["token"]:
            print("found ADJECTIVE that can be replaced : ", str(adjective["token"]))
            generate_fitness(sentence, adjective["token"], print_value=print_value)
            print("context fitness : ", adjective["fitness_matrix"])
            print("")
        if noun["token"]:
            print("found NOUN that can be replaced : ", str(noun["token"]))
            generate_fitness(sentence, noun["token"], print_value=print_value)
            print("context fitness : ", noun["fitness_matrix"])
            print("")

    return verb, adjective, noun

def modify_cover(mapping, replaceable_position, sentence, random_digits : list):
    zwsp = bytes.fromhex('e2808b').decode('utf-8')
    zwnj = bytes.fromhex('e2808c').decode('utf-8')

    zwc = ""

    for digit in random_digits:
        if digit=='0':
            zwc += zwsp
        else:
            zwc += zwnj

    words_in_sentence = sentence.split(' ')
    for position in replaceable_position:
        biner = random_digits.pop(0)
        if biner=='1':
            synonym = copy.deepcopy(mapping[position]["synonym"])
            synonym.remove(mapping[position]["max_fitness_word"])
            new_word = random.choice(synonym)
            words_in_sentence[position] = new_word
        
    modified_sentence = ' '.join(words_in_sentence)
    return modified_sentence, zwc

def generate_frequency_matrix(words : list):
    freq_matrix = []
    for word in words:
        freq_matrix.append((zipf_frequency(str(word), 'en', wordlist='large'), str(word)))
    
    return freq_matrix

def generate_encoder_code(words : list):
    if len(words)==1:
        print("cannot create encode table from 1 data. Minimum need to has 2 Data.", words)
        raise ValueError
    words_frequency_matrix = generate_frequency_matrix(words)
    encoder_code = huffman.generate_encoder_code(words_frequency_matrix)
    return encoder_code

def generate_decoder_code(words : list):
    words_frequency_matrix = generate_frequency_matrix(words)
    decoder_code = huffman.generate_decoder_code(words_frequency_matrix)
    return decoder_code

def print_coding_table(code):
    t = PrettyTable(['Secret Message', 'Word Replacement'])
    for key, value in code.items():
        t.add_row([key, value])
    print(t)

def get_max_fitness_sentence(mapping, replaceable_position, base_sentence):
    words_in_sentence = base_sentence.split(' ')
    for position in replaceable_position:
        max_word = mapping[position]["max_fitness_word"]
        words_in_sentence[position] = max_word
        
    max_sentence = ' '.join(words_in_sentence)
    return max_sentence

def calculate_value(mapping, replaceable_position, replace_words, original_sentence, reverse, zwc=None):
    words_in_sentence = original_sentence.split(' ')
    hidden_code = ""
    total_words = 0
    total_fitness = 0
    total_deviation = 0
    total_rate = 0
    replace_index = 0
    for position in replaceable_position:
        decoder = generate_decoder_code(mapping[position]["synonym"])
        replace_word = replace_words[replace_index]
        max_word = mapping[position]["max_fitness_word"]
        words_in_sentence[position] = replace_word
        hidden_code += decoder[replace_word]
        replace_fitness = mapping[position]["fitness_matrix"][replace_word]
        max_fitness = mapping[position]["fitness_matrix"][max_word]
        total_fitness += replace_fitness
        total_words += 1
        total_deviation += math.pow((replace_fitness-max_fitness), 2)

        if replace_word == max_word:
            total_rate += 1

        replace_index += 1

    sentence = ' '.join(words_in_sentence)
    label = "cover" if sentence==original_sentence else "stego"
    if label=="stego" and zwc:
        middle_index = math.ceil(len(words_in_sentence)/2)-1
        words_in_sentence[middle_index] = words_in_sentence[middle_index]+zwc

    sentence = ' '.join(words_in_sentence)
    CFM = (total_fitness/total_words)
    CFMD = (total_deviation/total_words)
    CFMR = (total_rate/total_words)
    if not reverse:
        code = 0 if sentence==original_sentence else 1
    else:
        code = 1 if sentence==original_sentence else 0

    return sentence, label, code, CFM, CFMD, CFMR

def validate_sentence(sentence):
    doc = nlp(sentence)

    verb_found = False
    adj_found = False
    noun_found = False

    for token in doc:
        if token.pos_=='VERB':
            verb_found = True
        elif token.pos_=='ADJ':
            adj_found = True
        elif token.pos_=='NOUN':
            noun_found = True
        elif token.tag_ == 'NNP' and not noun_found:
            try:
                noun_synonym = word2vec.get_synonym(token)
            except KeyError:
                continue
            noun_found = True

    if (verb_found or adj_found) and noun_found:
        return True
    else:
        return False

def build_dataset(formula, total, saving_file, reverse=False):
    import json
    import pandas as pd
    from generate_sentence import Generator
    animation = "|/-\\"
    generator = Generator()
    generator.add_field("SUBJECT", ["the NOUN", "PRON"])
    generator.add_field("OBJECT", ["NOUN ADJ ADV", "NOUN ADJ", "NOUN ADV", "ADJ ADV", "NOUN", "ADJ", "ADV"])
    dataset = {"sentences":[], "label":[], "code":[], "CFM":[], "CFMD":[], "CFMR":[]}

    total_cover = 1
    index = 0
    while total_cover<=total:
        try:
            #Generate random digits to modify cover text
            random_digits = ['0', '0']
            buff_random = []
            while (random_digits[0] == random_digits[1] == '0'):
                random_digits.clear()
                for i in range(5):
                    random_digits.append(qrandom.choice("01")) 

            buff_random = copy.deepcopy(random_digits)
 
            base_sentence = generator.generate(formula, 1)
            base_sentence = base_sentence[0]
            base_sentence = base_sentence.replace('.', '')

            if not validate_sentence(base_sentence):
                continue

            verb, adjective, noun = position_tagging(base_sentence)
            if not verb["token"] and not adjective["token"]:
                continue

            position_mapping = {verb["position"]:verb, adjective["position"]:adjective, noun["position"]:noun}
            replaceable_position = [verb["position"], adjective["position"], noun["position"]]
            replaceable_position = sorted([x for x in replaceable_position if x is not None])
            
            ori_sentence = get_max_fitness_sentence(position_mapping, replaceable_position, base_sentence)

            cover_text, zwc = modify_cover(position_mapping, replaceable_position, ori_sentence, random_digits)

            #calculate possible combinations
            combinations = []
            for synonym in position_mapping[replaceable_position[0]]["synonym"]:
                if len(replaceable_position)>=2:
                    for second_synonym in position_mapping[replaceable_position[1]]["synonym"]:
                        if len(replaceable_position)==3:
                            for third_synonym in position_mapping[replaceable_position[2]]["synonym"]:
                                combinations.append([str(synonym), str(second_synonym), str(third_synonym)])
                        else:
                            combinations.append([str(synonym), str(second_synonym)])
                else:
                    combinations.append([str(synonym)])

            for replace_words in combinations:
                sentence, label, code, CFM, CFMD, CFMR = calculate_value(position_mapping, replaceable_position, replace_words, cover_text, reverse, zwc=zwc)
                if sentence in dataset["sentences"]:
                    continue

                dataset["sentences"].append(sentence)
                dataset["label"].append(label)
                dataset["code"].append(code)
                dataset["CFM"].append(CFM)
                dataset["CFMD"].append(CFMD)
                dataset["CFMR"].append(CFMR)

            dataset["sentences"].append(ori_sentence)
            dataset["label"].append("original")
            dataset["code"].append("'"+''.join(map(str, buff_random)))
            dataset["CFM"].append("-")
            dataset["CFMD"].append("-")
            dataset["CFMR"].append("-")
            
            sys.stdout.write("\r")
            sys.stdout.flush()
            print(f"########## cover number {total_cover} ##########")
            print("Generated Sentence (from best_fitness.json) : ", base_sentence)
            print("Original Sentence (with max fitness context): ", ori_sentence)
            print("Random Biner : ", ''.join(map(str, buff_random)))
            print("Cover Sentence (modified from original using random biner) : ", cover_text)
            position_tagging(base_sentence, print_value=True)
            total_cover += 1
        except (KeyError, ValueError, UnboundLocalError) as error:
            index += 1
            sys.stdout.write("\r" + "retrying " + animation[index % len(animation)])
            sys.stdout.flush()
            continue

    df = pd.read_json(json.dumps(dataset))
    df.to_csv(saving_file)
    print("saving dataset to", saving_file)

def process_sentence(cover_sentence, secret_message):
    words_in_sentence = cover_sentence.split(' ')
    verb, adjective, noun = position_tagging(cover_sentence)
    if not verb["token"] and not adjective["token"]:
        sentence_structure(cover_sentence)
        print("Cannot found verb or adjective")
        raise ValueError

    if noun["position"] is None:
        sentence_structure(cover_sentence)
        print("Cannot found noun in the sentence")
        raise ValueError

    position_mapping = {verb["position"]:verb, adjective["position"]:adjective, noun["position"]:noun}

    first_word = {}
    second_word = {}

    if not verb["position"] is None:
        if verb["position"] < noun["position"]:
            first_word = verb
            second_word = noun
        else:
            first_word = noun
            second_word = verb
    elif not adjective["position"] is None:
        if adjective["position"] < noun["position"]:
            first_word = adjective
            second_word = noun
        else:
            first_word = noun
            second_word = adjective

    first_synonyms = first_word["synonym"]
    second_synonyms = second_word["synonym"]

    replaceable_position = [first_word["position"], second_word["position"]]

    first_code = generate_encoder_code(first_synonyms)
    second_code = generate_encoder_code(second_synonyms)

    start_position = 0
    end_position = 1
    first_word_replacement = ""
    second_word_replacement = ""
    while end_position <= len(secret_message):
        binary_part = secret_message[start_position:end_position]
        
        if not first_word_replacement and binary_part in first_code:
            first_word_replacement = first_code[binary_part]
            start_position = end_position
            end_position+=1
            continue

        if first_word_replacement and binary_part in second_code:
            second_word_replacement = second_code[binary_part]
            break

        end_position+=1

    if not first_word_replacement or not second_word_replacement:
        print("Cannot encode secret message to the sentence. Please follow coding table.")
        print("Coding table for first word")
        print_coding_table(first_code)
        print("\n")

        print("Coding table for second word")
        print_coding_table(second_code)
        print("\n")
        raise ValueError
    elif end_position<len(secret_message):
        print("Secret message is too long, cannot parse fully to the sentence.")
        print("Coding table for first word")
        print_coding_table(first_code)
        print("\n")

        print("Coding table for second word")
        print_coding_table(second_code)
        print("\n")
        raise ValueError
    
    replace_words = [first_word_replacement, second_word_replacement]
        
    total_words = 0
    total_fitness = 0
    total_deviation = 0
    total_rate = 0
    replace_index = 0
    for position in replaceable_position:
        replace_word = replace_words[replace_index]
        max_word = position_mapping[position]["max_fitness_word"]
        words_in_sentence[position] = replace_word
        replace_fitness = position_mapping[position]["fitness_matrix"][replace_word]
        max_fitness = position_mapping[position]["fitness_matrix"][max_word]
        total_fitness += replace_fitness
        total_words += 1
        total_deviation += math.pow((replace_fitness-max_fitness), 2)

        if replace_word == max_word:
            total_rate += 1

        replace_index += 1

    sentence = ' '.join(words_in_sentence)
    label = "cover" if sentence==cover_sentence else "stego"
    CFM = (total_fitness/total_words)
    CFMD = (total_deviation/total_words)
    CFMR = (total_rate/total_words)

    return sentence, label, CFM, CFMD, CFMR

def encoder(sentence : str, binary_message : str, total_synonym : int):
    verb, adjective, noun = position_tagging(sentence)
    
    if verb["position"] == adjective["position"]:
        sentence_structure(sentence)
        raise ValueError("Cannot found verb or adjective in the sentence. Or failed to get context fitness due to key error in word2vec.")

    if noun["position"] is None:
        sentence_structure(sentence)
        raise ValueError("Cannot found noun in the sentence. Or failed to get context fitness due to key error in word2vec.")

    first_word = {}
    second_word = {}

    if not verb["position"] is None:
        if verb["position"] < noun["position"]:
            first_word = verb
            second_word = noun
        else:
            first_word = noun
            second_word = verb
    elif not adjective["position"] is None:
        if adjective["position"] < noun["position"]:
            first_word = adjective
            second_word = noun
        else:
            first_word = noun
            second_word = adjective

    first_synonyms = first_word["synonym"]
    second_synonyms = second_word["synonym"]

    if total_synonym is None:
        pass
    elif total_synonym > len(first_synonyms) or total_synonym > len(second_synonyms):
        raise ValueError(f"Total synonym exceed available generated synonym. Don't use total-synonym option or use Max value = {total_synonym}")
    elif total_synonym:
        first_synonyms = first_synonyms[:total_synonym]
        second_synonyms = second_synonyms[:total_synonym]
        
    first_code = generate_encoder_code(first_synonyms)
    print("Coding table for first word")
    print_coding_table(first_code)
    print("\n")

    second_code = generate_encoder_code(second_synonyms)
    print("Coding table for second word")
    print_coding_table(second_code)
    print("\n")

    start_position = 0
    end_position = 1
    first_word_replacement = ""
    second_word_replacement = ""
    while end_position <= len(binary_message):
        binary_part = binary_message[start_position:end_position]
        
        if not first_word_replacement and binary_part in first_code:
            first_word_replacement = first_code[binary_part]
            start_position = end_position
            end_position+=1
            continue

        if first_word_replacement and binary_part in second_code:
            second_word_replacement = second_code[binary_part]
            break

        end_position+=1
    
    if not first_word_replacement or not second_word_replacement:
        raise ValueError("Cannot encode secret message to the sentence. Please follow coding table.")
    elif end_position<len(binary_message):
        raise ValueError("Secret message is too long, cannot parse fully to the sentence.")

    words_in_sentence = sentence.split(' ')
    words_in_sentence[first_word["position"]] = first_word_replacement
    words_in_sentence[second_word["position"]] = second_word_replacement

    return ' '.join(words_in_sentence)

def decoder(sentence : str, total_synonym : int):
    verb, adjective, noun = position_tagging(sentence)
    
    if verb["position"] == adjective["position"]:
        sentence_structure(sentence)
        raise ValueError("Cannot found verb or adjective in the sentence. Or failed to get context fitness due to key error in word2vec.")

    if not noun["position"]:
        sentence_structure(sentence)
        raise ValueError("Cannot found noun in the sentence. Or failed to get context fitness due to key error in word2vec.")

    first_word = {}
    second_word = {}

    if verb["position"] :
        if verb["position"] < noun["position"]:
            first_word = verb
            second_word = noun
        else:
            first_word = noun
            second_word = verb
    elif adjective["position"]:
        if adjective["position"] < noun["position"]:
            first_word = adjective
            second_word = noun
        else:
            first_word = noun
            second_word = adjective

    first_synonyms = first_word["synonym"]
    second_synonyms = second_word["synonym"]

    if total_synonym is None:
        pass
    elif total_synonym > len(first_synonyms) or total_synonym > len(second_synonyms):
        raise ValueError(f"Total synonym exceed available generated synonym. Don't use total-synonym option or use Max value = {total_synonym}")
    elif total_synonym:
        first_synonyms = first_synonyms[:total_synonym]
        second_synonyms = second_synonyms[:total_synonym]

    first_code = generate_decoder_code(first_synonyms)
    print("Coding table for first word")
    print_coding_table(first_code)
    print("\n")

    second_code = generate_decoder_code(second_synonyms)
    print("Coding table for second word")
    print_coding_table(second_code)
    print("\n")

    secret_message = first_code[str(first_word["token"])] + second_code[str(second_word["token"])]
    return secret_message
