from nltk.corpus import gutenberg, stopwords, wordnet
from nltk.corpus.reader.plaintext import concat
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import string
import pandas as pd
import gensim
import logging
import json
import re

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

try:
    with open("./synonyms.json", 'r') as json_file:
        synonyms_table = json.load(json_file)
except FileNotFoundError:
    print("synonyms json not found, please generate 'synonyms.json' first. Use build()")

class Word2Vec():
    def __init__(self):
        self.wl = WordNetLemmatizer()
        try:
            self.vector_model = gensim.models.KeyedVectors.load("word2vec.wordvectors", mmap='r')
        except FileNotFoundError:
            logging.error("Word Vector not found, please generate 'word2vec.wordvectors' first. Use generate_word2vec()")

        try:
            with open("./tfidf_dict.json", 'r') as json_file:
                self.tfidf_dict = json.load(json_file)
        except FileNotFoundError:
            logging.error("Tfidf json not found, please generate 'tfidf_dict.json' first. Use generate_tfidf()")

    def sentences_from_corpus(self, corpus, fileids = None):

        def read_sent_block(stream):
            sents = []
            for para in corpus._para_block_reader(stream):
                sents.extend([s.replace('\n', ' ') for s in corpus._sent_tokenizer.tokenize(para)])
            return sents

        return concat([corpus.CorpusView(path, read_sent_block, encoding=enc)
                    for (path, enc, fileid)
                    in corpus.abspaths(fileids, True, True)])

    def generate_bag_of_word(self):
        for file_id in gutenberg.fileids():
            logging.info("reading file {0}...this may take a while".format(file_id))
            for sentence in self.sentences_from_corpus(gutenberg, file_id):
                sentence = self.lemmatizer(self.stopword(self.preprocess_text(sentence)))
                yield gensim.utils.simple_preprocess(sentence)

    def generate_word2vec(self):
        documents = list(self.generate_bag_of_word())
        logging.info("Done reading data file")
        model = gensim.models.Word2Vec(documents, vector_size=150, window=5, min_count=2, workers=10)
        model.train(documents,total_examples=len(documents),epochs=10)
        word_vectors = model.wv
        word_vectors.save("word2vec.wordvectors")
        logging.info("Done training, saving model to word2vec.wordvectors file")
        self.vector_model = word_vectors

    def generate_tfidf(self):
        logging.info("Reading file, for word count...this may take a while")
        documents = []
        for file_id in gutenberg.fileids():
            documents.append(gutenberg.raw(file_id))
        #instantiate CountVectorizer() ignore english stop words and ignore if word only show in less than 2 documents
        cv=CountVectorizer(stop_words=stopwords.words('english'), min_df=2, preprocessor=self.preprocess_text) 
        
        # this steps generates word counts for the words in your docs 
        word_count_vector=cv.fit_transform(documents)
        	
        tfidf_transformer=TfidfTransformer(smooth_idf=True, use_idf=True) 
        tfidf_transformer.fit(word_count_vector)

        # count matrix 
        count_vector = cv.transform(documents)
        feature_names = cv.get_feature_names_out()

        count_df = pd.DataFrame(count_vector.toarray(), index=gutenberg.fileids(), columns=feature_names)
        count_df.to_csv("word_count.csv")
        logging.info("saving word count table to word_count.csv")
        
        # tf-idf scores 
        tf_idf_vector=tfidf_transformer.transform(count_vector)
        
        tfidf_df = pd.DataFrame(tf_idf_vector.toarray(), columns=feature_names, index=gutenberg.fileids())
        tfidf_df.to_csv("tfidf.csv")
        logging.info("saving tfidf table to tfidf.csv")

        # We will generate dictionary for each word that contain max tfidf inside vector
        df = pd.DataFrame(tf_idf_vector.toarray(), columns=feature_names)
        self.tfidf_dict = {}
        for word in feature_names:
            self.tfidf_dict[word] = df.get(word).max()

        with open("tfidf_dict.json", 'w') as json_file: 
            json.dump(self.tfidf_dict, json_file)
        
        logging.info("saving tfidf dictionary to tfidf_dict.json")

    # This is a helper function to map NTLK position tags
    def get_wordnet_pos(self, tag):
        if tag.startswith('J'):
            return wordnet.ADJ
        elif tag.startswith('V'):
            return wordnet.VERB
        elif tag.startswith('N'):
            return wordnet.NOUN
        elif tag.startswith('R'):
            return wordnet.ADV
        else:
            return wordnet.NOUN

    # STOPWORD REMOVAL
    def stopword(self, text):
        a= [i for i in text.split() if i not in stopwords.words('english')]
        return ' '.join(a)

    def lemmatizer(self, text):
        word_pos_tags = pos_tag(word_tokenize(text)) # Get position tags
        a=[self.wl.lemmatize(tag[0], self.get_wordnet_pos(tag[1])) for idx, tag in enumerate(word_pos_tags)] # Map the position tag and lemmatize the word/token
        return " ".join(a)

    def preprocess_text(self, text:str):
        # Here we convert to lowercase, strip and remove punctuations
        text = text.lower()
        text=text.strip()  
        text=re.compile('<.*?>').sub('', text) 
        text = re.compile('[%s]' % re.escape(string.punctuation)).sub(' ', text)  
        text = re.sub('\s+', ' ', text)  
        text = re.sub(r'\[[0-9]*\]',' ',text) 
        text=re.sub(r'[^\w\s]', '', str(text).lower().strip())
        text = re.sub(r'\d',' ',text) 
        text = re.sub(r'\s+',' ',text)
        return text

    def cosine_similarity(self, word_1 : str, word_2 : str):
        return self.vector_model.similarity(w1=word_1, w2=word_2)

    def get_synonym(self, target_word, max_count:int=3, threshold:float=0.3):
        global synonyms_table
        word = str(target_word).lower()
        synonyms = []
        try:
            result = synonyms_table[word]
            synonyms = result["synonym"][0]
        except KeyError:
            try:
                vector_synonyms = self.vector_model.most_similar(str(target_word), topn=max_count)

                synonyms = [word]
                for synonym, similarity in vector_synonyms:
                    if similarity<threshold:
                        break
                    synonyms.append(str(synonym))
                category = "Vector"
            except KeyError:
                synset = target_word._.wordnet.synsets()
                category = "Wordnet"
                if synset:
                    synonyms = [syn.lower() for syn in synset[0].lemma_names()]
                    synonyms.insert(0, word)

            score = 1
            if not synonyms:
                raise KeyError
            for synonym in synonyms:
                
                if not synonym in synonyms_table:
                    synonyms_table[synonym] = {"synonym":[], "category":[], "pred_score":[]}

                if category in synonyms_table[synonym]["category"]:
                    continue

                synonyms_table[synonym]["synonym"].insert(0, synonyms)
                synonyms_table[synonym]["category"].insert(0, category)
                synonyms_table[synonym]["pred_score"].insert(0, score)

            with open("synonyms.json", 'w') as json_file: 
                json.dump(synonyms_table, json_file)

        return synonyms

    def context_fitness(self, sentence:str, target_word, window_size:int=3, print_value=False):
        sentence_array = sentence.split()
        target_index = sentence_array.index(str(target_word))
        start_index = 0 if (target_index-window_size)<0 else target_index-window_size
        end_index = len(sentence_array) if (target_index+window_size)>(len(sentence_array)-1) else target_index+window_size
        synonyms = self.get_synonym(target_word)

        result = {}
        for synonym in synonyms:
            fitness = 0
            for index in range(start_index, end_index):
                word = sentence_array[index]
                if index == target_index:
                    continue
                try:
                    fitness += self.cosine_similarity(synonym, word)*self.tfidf_dict[word]
                    if print_value:
                        print("Cosine similarity between :", synonym, "and", word, "is", self.cosine_similarity(synonym, word))
                except KeyError:
                    if print_value:
                        print("key error, cannot calculate cosine similarity between", synonym, "and", word)
                    pass
            result[synonym] = fitness

        return result
