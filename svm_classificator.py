# Support Vector Machine
# Importing the libraries
import json
import pathlib
import pandas as pd
from timeit import default_timer as timer
from prettytable import PrettyTable
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix
import qrandom
import matplotlib.pyplot as plt
import seaborn as sn
from joblib import dump, load

def get_classification_report(dataset_file, test_size, stego_ratio=1):

    # Importing the datasets
    extension = pathlib.Path(dataset_file).suffix
    if extension=='.xls' or extension=='xlsx':
        dataset = pd.read_excel(dataset_file)
    elif extension=='.csv':
        dataset = pd.read_csv(dataset_file)
    else:
        return "Unknown file format, this function only process Excel(xls or xlsx) or CSV file."

    X_ori = dataset.iloc[:, [4,5,6]].values
    Y_ori = dataset.iloc[:, 3].values
    sentences_ori = dataset.iloc[:, 1].values

    X = []
    Y = []
    sentences = []
    total_cover = (Y_ori == '0').sum()
    total_stego = total_cover*stego_ratio
    count_stego = 0

    # exclude original dataset from being calculated
    for i in range(len(Y_ori)):
        if not (Y_ori[i]=='1' or Y_ori[i]=='0'):
            continue
        else:
            if Y_ori[i]=='1' and count_stego >= total_stego:
                continue
            count_stego += 1
            X.append([float(val) for val in X_ori[i]])
            Y.append(int(Y_ori[i]))
            sentences.append(sentences_ori[i])

    # Splitting the dataset into the Training set and Test set
    X_Train, X_Test, Y_Train, Y_Test, sentences_train, sentences_test = train_test_split(X, Y, sentences, test_size = test_size, random_state = 0)

    # Invert the stego-cover in the test array
    for i in range(len(Y_Test)):
        if Y_Test[i]==0:
            Y_Test[i]=1
        else:
            Y_Test[i]=0

    # Feature Scaling
    sc_X = StandardScaler()
    X_Train = sc_X.fit_transform(X_Train)
    X_Test = sc_X.transform(X_Test)

    # Fitting the classifier into the Training set
    classifier = SVC(kernel = 'linear', random_state = 0)
    classifier.fit(X_Train, Y_Train)

    # Predicting the test set 
    print("Sentences to be tested is :")
    for sentence in sentences_test:
        print(sentence)
    Y_Pred = classifier.predict(X_Test)

    # Saving SVM model
    print("\nSaving SVM model to svm_model.joblib")
    dump(classifier, 'svm_model.joblib') 

    # Generate classification report
    print("\nClassification Report :")
    print(classification_report(Y_Test, Y_Pred, zero_division=0))

    cm = confusion_matrix(Y_Test, Y_Pred)
 
    # Draw confusion matrix
    plt.figure (figsize=(10,7))
    sn.heatmap(cm, annot=True)
    plt.xlabel('Predicted')
    plt.ylabel('Truth')
    plt.show()

def inference(CFM, CFMD, CFMR):
    try:
        classifier = load('svm_model.joblib')
    except FileNotFoundError:
        print("please generate svm model by using command 'python app_cli.py svm --file dataset.csv'")
        return

    prediction = classifier.predict([[CFM, CFMD, CFMR]])

    if prediction[0]==0:
        print("The sentence is Cover")
    else:
        print("The sentence is Stego")

def test_dataset(dataset_file):
    try:
        classifier = load('svm_model.joblib')
    except FileNotFoundError:
        print("please generate svm model by using command 'python app_cli.py svm --file dataset.csv'")
        return

    # Importing the datasets
    extension = pathlib.Path(dataset_file).suffix
    if extension=='.xls' or extension=='xlsx':
        dataset = pd.read_excel(dataset_file)
    elif extension=='.csv':
        dataset = pd.read_csv(dataset_file)
    else:
        return "Unknown file format, this function only process Excel(xls or xlsx) or CSV file."

    X_ori = dataset.iloc[:, [4,5,6]].values # this contain CFM, CFMD, CFMR
    Y_ori = dataset.iloc[:, 3].values # this contain code column inside dataset
    sentences_ori = dataset.iloc[:, 1].values # this contain the dataset sentences

    X = []
    Y = []
    sentences = []

    # exclude original dataset from being calculated
    for i in range(len(Y_ori)):
        if Y_ori[i]=='1' or Y_ori[i]=='0':
            X.append([float(val) for val in X_ori[i]])
            Y.append(int(Y_ori[i]))
            sentences.append(sentences_ori[i])

    # Invert the stego-cover in the test array
    for i in range(len(Y)):
        if Y[i]==0:
            Y[i]=1
        else:
            Y[i]=0

    # Feature Scaling
    sc_X = StandardScaler()
    sc_X.fit(X)
    X_Test = sc_X.transform(X)

    # Predicting the test set 
    print("Sentences to be tested is :")
    for sentence in sentences:
        print(sentence)
    Y_Pred = classifier.predict(X_Test)

    # Generate classification report
    print("\nClassification Report :")
    print(classification_report(Y, Y_Pred, zero_division=0))

    cm = confusion_matrix(Y, Y_Pred)
 
    # Draw confusion matrix
    plt.figure (figsize=(10,7))
    sn.heatmap(cm, annot=True)
    plt.xlabel('Predicted')
    plt.ylabel('Truth')
    plt.show()

def validate(dataset_file, test_size, secret_message, savename):
    from t_lex import process_sentence, decoder

    try:
        classifier = load('svm_model.joblib')
    except FileNotFoundError:
        print("please generate svm model by using command 'python app_cli.py svm --file dataset.csv'")
        return

    # Importing the datasets
    extension = pathlib.Path(dataset_file).suffix
    if extension=='.xls' or extension=='xlsx':
        dataset = pd.read_excel(dataset_file)
    elif extension=='.csv':
        dataset = pd.read_csv(dataset_file)
    else:
        return "Unknown file format, this function only process Excel(xls or xlsx) or CSV file."

    Y_ori = dataset.iloc[:, 3].values # this contain code column inside dataset
    sentences_ori = dataset.iloc[:, 1].values # this contain the dataset sentences

    cover_sentences = []
    original_sentences = []

    # exclude original and stego dataset from being calculated
    for i in range(len(Y_ori)):
        if Y_ori[i]=='0':
            cover_sentences.append(sentences_ori[i])
        elif Y_ori[i]=='1':
            continue
        else:
            original_sentences.append(sentences_ori[i])
    if test_size<1:
        _, sentences_test, _, original_test = train_test_split(cover_sentences, original_sentences, test_size = test_size, random_state = 0)
    else:
        sentences_test = cover_sentences
        original_test = original_sentences

    t = PrettyTable(['No', 'Cover', 'Stego', 'Secret Message', 'Real', 'Prediction', 'Validation'])

    header_row_1 = ['', '', '', '', '', 'Prediction', 'Prediction', 'Real', 'Real', 'Validation', 'Validation', 'Embed Processing Time', 'Embed Processing Time', 'Extract Processing Time', 'Extract Processing Time'] 
    header_row_2 = ['Original Cover', 'Stego Previous', 'Modified Cover', 'Stego Proposed', 'Secret Message', 'Previous', 'Proposed', 'Previous', 'Proposed', 'Previous', 'Proposed', 'Previous', 'Proposed', 'Previous', 'Proposed']  
    data = []
    tuples = list(zip(header_row_1, header_row_2)) 
    cols = pd.MultiIndex.from_tuples(tuples)
    
    
    dataset_validated = {"cover":[], "stego":[], "secret message":[], "real":[], "prediction":[], "validation":[]}
    performance_matrix = {"TN":0, "FP":0, "FN":0, "TP":0}
    prev_performance_matrix = {"TN":0, "FP":0, "FN":0, "TP":0}
    row_num = 1
    for i in range(len(sentences_test)):
        strencode = sentences_test[i].encode("ascii", "ignore")
        ori_strencode = original_test[i].encode("ascii", "ignore")
 
        cover = strencode.decode()
        original = ori_strencode.decode()
        print(f"Original Cover Sentence : {original}")
        print(f"Modified Cover Sentence : {cover}")
        print(f"Secret Message : {secret_message}")
        try:
            start = timer()
            stego, label, CFM, CFMD, CFMR = process_sentence(cover, secret_message)
            proposed_embed_time = timer() - start
            
            start = timer()
            decoder(stego, None)
            proposed_extract_time = timer() - start

            start = timer()
            original_stego, original_label, original_CFM, original_CFMD, original_CFMR = process_sentence(original, secret_message)
            previous_embed_time = timer() - start
            
            start = timer()
            decoder(original_stego, None)
            previous_extract_time = timer() - start
        except (KeyError, ValueError):
            print("<=====================================================>")
            continue
        '''
        # generate random stego proposed
        stego_found = False
        while not stego_found:
            #Generate random digits to generate random secret
            random_digits = []
            for i in range(len(secret_message)):
                random_digits.append(qrandom.choice("01"))

            random_secret = ''.join(map(str, random_digits))
            if random_secret==secret_message:
                continue

            try:
                random_sentence, _, _, _, _ = process_sentence(cover, random_secret)
                stego_found = True
            except (KeyError, ValueError):
                continue
        '''
        print(f"Sentence : {stego}")
    
        print(f"Real : {label}")
        prediction = classifier.predict([[CFM, CFMD, CFMR]])
        ori_prediction = classifier.predict([[original_CFM, original_CFMD, original_CFMR]])

        if prediction[0]==0:
            prediction_result = "cover"
        else:
            prediction_result = "stego"
        if ori_prediction[0]==0:
            ori_prediction_result = "cover"
        else:
            ori_prediction_result = "stego"

        if original==cover:
            pass
        else:
            prediction_result = "stego" if prediction_result=="cover" else "cover"
        print(f"Prediction : {prediction_result}")
        validation = "correct" if label==prediction_result else "wrong"
        ori_validation = "correct" if original_label==ori_prediction_result else "wrong"
        print(f"Validation : {validation}")
        print("<=====================================================>")

        data.append([original, original_stego, cover, stego, secret_message, ori_prediction_result, prediction_result, original_label, label, ori_validation, validation, previous_embed_time, proposed_embed_time, previous_extract_time, proposed_extract_time])

        # previous
        dataset_validated["cover"].append(cover)
        dataset_validated["stego"].append(stego)
        dataset_validated["secret message"].append(secret_message)
        dataset_validated["real"].append(label)
        dataset_validated["prediction"].append(prediction_result)
        dataset_validated["validation"].append(validation)

        t.add_row([row_num, cover, stego, secret_message, label, prediction_result, validation])
        row_num += 1

        # proposed
        dataset_validated["cover"].append(cover)
        dataset_validated["stego"].append(stego)
        dataset_validated["secret message"].append(secret_message)
        dataset_validated["real"].append(label)
        dataset_validated["prediction"].append(prediction_result)
        dataset_validated["validation"].append(validation)

        t.add_row([row_num, cover, stego, secret_message, label, prediction_result, validation])
        row_num += 1

        # record performance matrix
        if prediction_result=="cover" and  label=="cover":
            performance_matrix["TN"] += 1
        elif prediction_result=="stego" and label=="cover":
            performance_matrix["FP"] +=1
        elif prediction_result=="cover" and label=="stego":
            performance_matrix["FN"] +=1
        elif prediction_result=="stego" and label=="stego":
            performance_matrix["TP"] +=1

        if ori_prediction_result=="cover" and  original_label=="cover":
            prev_performance_matrix["TN"] += 1
        elif ori_prediction_result=="stego" and original_label=="cover":
            prev_performance_matrix["FP"] +=1
        elif ori_prediction_result=="cover" and original_label=="stego":
            prev_performance_matrix["FN"] +=1
        elif ori_prediction_result=="stego" and original_label=="stego":
            prev_performance_matrix["TP"] +=1

    # calculate performance proposed
    TN = performance_matrix["TN"]
    FP = performance_matrix["FP"]
    FN = performance_matrix["FN"]
    TP = performance_matrix["TP"]
    
    accuracy = (TP+TN) / (TP+FP+FN+TN)
    try:
        precission = TP / (TP+FP)
    except ZeroDivisionError:
        precission = 0

    try:
        recall = TP / (TP+FN)
    except ZeroDivisionError:
        recall = 0
    
    try:
        f_value = 2 * ((recall*precission) / (recall+precission))
    except ZeroDivisionError:
        f_value = 0

    data.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['Matrix Confusion and Performance Measure of Proposed Method', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['TN', TN, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['FP', FP, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['TP', TP, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['FN', FN, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['accuracy', f"{accuracy*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['precission', f"{precission*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['recall', f"{recall*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['f_value', f"{f_value*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])

    # calculate performance previous
    TN = prev_performance_matrix["TN"]
    FP = prev_performance_matrix["FP"]
    FN = prev_performance_matrix["FN"]
    TP = prev_performance_matrix["TP"]
    
    accuracy = (TP+TN) / (TP+FP+FN+TN)
    try:
        precission = TP / (TP+FP)
    except ZeroDivisionError:
        precission = 0
        
    try:
        recall = TP / (TP+FN)
    except ZeroDivisionError:
        recall = 0
    
    try:
        f_value = 2 * ((recall*precission) / (recall+precission))
    except ZeroDivisionError:
        f_value = 0

    data.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['Matrix Confusion and Performance Measure of Previous Method', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['TN', TN, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['FP', FP, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['TP', TP, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['FN', FN, '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['accuracy', f"{accuracy*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['precission', f"{precission*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['recall', f"{recall*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])
    data.append(['f_value', f"{f_value*100:.2f}%", '', '', '', '', '', '', '', '', '', '', '', '', ''])

    print(t)
    df_table = pd.DataFrame(data, columns = cols)
    df_table.index = df_table.index + 1
    df_table.to_html(savename+".html")
    df_table.to_excel(savename+".xlsx")
    saving_file = savename+".csv"
    df = pd.read_json(json.dumps(dataset_validated))
    df.to_csv(saving_file)
    print(f"saving to {savename}.csv, {savename}.html and {savename}.xlsx")
