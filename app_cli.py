import typer
import t_lex
from generate_sentence import Generator
from fitness_context import Word2Vec

APP_NAME = "T-Lex System With Huffman Coding"
app = typer.Typer(name=APP_NAME)

def binary_validation_callback(value: str):
    try:
        int(value, 2)
    except ValueError:
        raise typer.BadParameter("Message contain wrong binary format. Make sure to fill '1' and '0' combination only. Example '1100111'.")
    return value

@app.command()
def encode(sentence: str = typer.Argument(..., help="Sentence to modified must contain verb or adjective and must have noun"),
           secret_message: str = typer.Argument(..., callback=binary_validation_callback, help="Fill secret message using binary format"),
           total_synonym: int = typer.Option(None, help="Number of synonyms that need to generate")):
    '''
    Encode secret message to the sentence by following huffman code
    '''
    try:
        result = t_lex.encoder(sentence, secret_message, total_synonym)
        typer.echo(f"Encoded Message = {result}")
    except BaseException as e:
        typer.echo(str(e))

@app.command()
def decode(sentence: str = typer.Argument(..., help="Sentence that you want to find out secret message behind it"),
           total_synonym: int = typer.Option(None, help="Number of synonyms that need to generate")):
    '''
    Decode sentence to get secret message by following huffman code
    '''
    try:
        result = t_lex.decoder(sentence, total_synonym)
        typer.echo(f"Secret Message = {result}")
    except BaseException as e:
        typer.echo(str(e))

@app.command()
def generate(formula: str = typer.Argument(..., help="Grammar formula to generate sentences"), total: int = typer.Option(1, help="Number of sentence that need to generate"), file: str = typer.Option(None, help="File name to save generated sentences")):
    '''
    Generate sentences using context-free grammars.

    Pre-defined custom field is {"SUBJECT":["the NOUN", "PRON"], "OBJECT":["NOUN ADJ ADV", "NOUN ADJ", "NOUN ADV", "ADJ ADV", "NOUN", "ADJ", "ADV"]}

    You can use this example python app_cli.py generate "SUBJECT VERB OBJECT"
    '''
    generator = Generator()
    generator.add_field("SUBJECT", ["the NOUN", "PRON"])
    generator.add_field("OBJECT", ["NOUN ADJ ADV", "NOUN ADJ", "NOUN ADV", "ADJ ADV", "NOUN", "ADJ", "ADV"])
    
    typer.echo("\nGenerate sentence results:")
    total_sentence = 1
    while total_sentence<=total:
        sentence = generator.generate(formula, 1)[0]
        if not t_lex.validate_sentence(sentence):
            continue
        
        try:
            verb, adjective, noun = t_lex.position_tagging(sentence)
        except (KeyError, ValueError, UnboundLocalError) as error:
            continue

        if not verb["token"] and not adjective["token"]:
            print("no verb and adjective")
            print(sentence)
            t_lex.sentence_structure(sentence)
            continue

        if not noun["token"]:
            print("no noun")
            print(sentence)
            t_lex.sentence_structure(sentence)
            continue

        if not file:
            typer.echo(sentence)
        else:
            with open(file, 'w') as saving_file:
                saving_file.write(sentence+" ")

        total_sentence += 1

    if file:    
        typer.echo(f"Sentences is saved in {file}")

@app.command()
def build(formula: str = typer.Argument(..., help="Grammar formula to generate sentences"),
        total_cover: int = typer.Option(1, help="Number of cover sentence that need to generate"),
        file: str = typer.Option(None, help="File name to save generated sentences"),
        reverse: bool = typer.Option(False, help="File name to save generated sentences")):
    '''
    Generate dataset from cover sentences using context-free grammars.

    Pre-defined custom field is {"SUBJECT":["the NOUN", "PRON"], "OBJECT":["NOUN ADJ ADV", "NOUN ADJ", "NOUN ADV", "ADJ ADV", "NOUN", "ADJ", "ADV"]}

    You can use this example python app_cli.py build "SUBJECT VERB OBJECT" --total-cover 1000 --file dataset.csv
    '''
    t_lex.build_dataset(formula, total_cover, file, reverse)

@app.command()
def train_svm(file: str = typer.Option(None, help="Dataset file in Microsoft Excel or CSV format"), 
            test_size: float = typer.Option(0.3, help="Test percentage to split the dataset"), 
            stego_ratio: float = typer.Option(1.3, help="To prevent imbalance, we can choose how much stego included in train data using stego_ratio. total_stego = total_cover*stego_ratio")):
    '''
    Generate classification report from produced dataset.

    this function will split dataset into training data and test data, following percentage from test-size(by default it's 30%). While in the test data, stego-cover will be inverted.

    You can use this example python app_cli.py train-svm --file dataset.xlsx --test-size 0.3
    '''
    if not file:
        typer.echo("Need to add dataset file using option --file dataset.csv")
        raise typer.Abort()
    from svm_classificator import get_classification_report
    get_classification_report(file, test_size, stego_ratio)

@app.command()
def test_svm(file: str = typer.Option(None, help="Dataset file in Microsoft Excel or CSV format")):
    '''
    Generate classification report from produced dataset.

    stego-cover will be inverted.

    You can use this example python app_cli.py test-svm --file dataset.xlsx
    '''
    if not file:
        typer.echo("Need to add dataset file using option --file dataset.csv")
        raise typer.Abort()
    from svm_classificator import test_dataset
    test_dataset(file)

@app.command()
def validate_dataset(file: str = typer.Option(None, help="Dataset file in Microsoft Excel or CSV format"),
                    test_size: float = typer.Option(0.3, help="Test percentage of cover inside dataset"),
                    secret_message: str = typer.Option(..., callback=binary_validation_callback, help="Fill secret message using binary format"),
                    savename : str = typer.Option(None, help="Saving validation result to excel, csv and html")):
    '''
    Generate classification report from produced dataset.

    result is saved in csv with format
    no | sentences |secret message | real | prediction | validation

    You can use this example python app_cli.py validate-dataset --file dataset.xlsx --test-size 0.3  --secret-message 0011 --savename validation_test
    '''
    if not file:
        typer.echo("Need to add dataset file using option --file dataset.csv")
        raise typer.Abort()
    if not savename:
        typer.echo("Need to add saving file name using option --savename saving_name")
        raise typer.Abort()
    from svm_classificator import validate
    validate(file, test_size, secret_message, savename)

@app.command()
def inference(cfm: float = typer.Option(None, help="Context Fitness Mean"), cfmd: float = typer.Option(None, help="Context Fitness Maximum Deviation"), cfmr: float = typer.Option(None, help="Context Fitness Maximum Rate")):
    '''
    Try to validate SVM model by passing CFM, CFMD and CFMR to predict if sentence is stego or cover

    This function will predict stego or cover by passing value to SVM model
    '''
    if not isinstance(cfm, float):
        typer.echo("Need to input CFM value using '--cfm cfm_number'")
        raise typer.Abort()
    if not isinstance(cfmd, float):
        typer.echo("Need to input CFMD value using '--cfmd cfmd_number'")
        raise typer.Abort()
    if not isinstance(cfmr, float):
        typer.echo("Need to input CFMR value using '--cfmr cfmr_number'")
        raise typer.Abort()

    from svm_classificator import inference
    inference(CFM=cfm, CFMD=cfmd, CFMR=cfmr)

@app.command()
def initialize():
    '''
    Initialize all required files. 

    ⚠️ Warning, this could replace all the generated files from latest training. Make sure to backup your data before execute this command.⚠️
    '''
    replace = typer.confirm("Are you sure you want to replace all the training results such as 'word2vec.wordvectors', 'tfidf_dict.json' and 'best_fitness.json'?")
    if not replace:
        typer.echo("Operation is canceled")
        raise typer.Abort()
    typer.echo("Generating training files ...this may take a while")
    word2vec = Word2Vec()
    word2vec.generate_word2vec()
    word2vec.generate_tfidf()
    generator = Generator()
    generator.build()
    typer.echo("Operation completed 🎉")
    
if __name__ == "__main__":
    app()
 