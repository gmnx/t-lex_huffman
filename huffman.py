def assign_code(nodes, label, result, encoder_code, prefix = ''):    
    childs = nodes[label]     
    tree = {}
    if len(childs) == 2:
        tree['0'] = assign_code(nodes, childs[0], result,  encoder_code, prefix+'0')
        tree['1'] = assign_code(nodes, childs[1], result,  encoder_code, prefix+'1')     
        return tree
    else:
        result[label] = prefix
        encoder_code[prefix] = label
        return label

def Huffman_code(_vals):    
    vals = _vals.copy()
    nodes = {}
    for n in vals.keys(): # leafs initialization
        nodes[n] = []

    while len(vals) > 1: # binary tree creation
        s_vals = sorted(vals.items(), key=lambda x:x[1]) 
        a1 = s_vals[0][0]
        a2 = s_vals[1][0]
        vals[a1+a2] = vals.pop(a1) + vals.pop(a2)
        nodes[a1+a2] = [a1, a2]        
    code = {}
    encoder_code = {}
    root = a1+a2
    tree = {}
    tree = assign_code(nodes, root, code, encoder_code)   # assignment of the code for the given binary tree      
    return code, encoder_code, tree

def generate_encoder_code(frequency_matrix):
    vals = {l:v for (v,l) in frequency_matrix}
    code, encoder_code, tree = Huffman_code(vals)
    return encoder_code

def generate_decoder_code(frequency_matrix):
    vals = {l:v for (v,l) in frequency_matrix}
    code, encoder_code, tree = Huffman_code(vals)
    return code
