import spacy
from spacy_wordnet.wordnet_annotator import WordnetAnnotator
import logging
import json
import random
from nltk.corpus import gutenberg, stopwords
from fitness_context import Word2Vec

nlp = spacy.load("en_core_web_lg")
nlp.add_pipe("spacy_wordnet", after='tagger', config={'lang': nlp.lang})
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

class Generator():
    def __init__(self) -> None:
        self.word2vec = Word2Vec()

        try:
            with open("./best_fitness.json", 'r') as json_file:
                self.best_fitness = json.load(json_file)
        except FileNotFoundError:
            logging.error("best_fitness json not found, please generate 'best_fitness.json' first. Use build()")

        try:
            with open("./synonyms.json", 'r') as json_file:
                self.synonyms = json.load(json_file)
        except FileNotFoundError:
            logging.error("synonyms json not found, please generate 'synonyms.json' first. Use build()")

    def build(self):
        dataset = self.word2vec.generate_bag_of_word()
        self.best_fitness = {}

        for file_id in gutenberg.fileids():
            logging.info("reading file {0}...this may take a while".format(file_id))
            for sentence in self.word2vec.sentences_from_corpus(gutenberg, file_id):
                sentence = self.word2vec.lemmatizer(self.word2vec.preprocess_text(sentence))
                doc = nlp(sentence)
                for token in doc:
                    category = token.pos_
                    target_word = str(token)
                    if category not in self.best_fitness:
                        self.best_fitness[category] = []
                    if target_word not in stopwords.words('english'):
                        try:
                            fitness_result = self.word2vec.context_fitness(sentence, target_word)
                        except KeyError:
                            self.best_fitness[category].append(target_word)
                        except ValueError:
                            continue
                        higher_fitness = max(fitness_result, key=fitness_result.get)
                        self.best_fitness[category].append(str(higher_fitness))
                    else:
                        self.best_fitness[category].append(target_word)

        for category, words in self.best_fitness.items():
            self.best_fitness[category] = list(set(words))

        with open("best_fitness.json", 'w') as json_file: 
            json.dump(self.best_fitness, json_file)
        
        logging.info("saving best fitness dictionary to best_fitness.json")

        self.build_synonyms()

    def build_synonyms(self):
        from text_classificator import classify_text
        logging.info("Building synonyms table...\n")
        self.synonyms = {}
        acceptable_position = ["PRON", "NOUN", "NNP", "ADJ", "VERB", "ADV"]

        for file_id in gutenberg.fileids():
            logging.info("reading file {0}...this may take a while".format(file_id))
            for sentence in self.word2vec.sentences_from_corpus(gutenberg, file_id):
                sentence = self.word2vec.preprocess_text(sentence)

                try:
                    classify_result = classify_text(sentence)
                except RuntimeError:
                    continue
                category = classify_result["label"]
                score = classify_result["score"]
                sentence = self.word2vec.lemmatizer(sentence)
                doc = nlp(sentence)
                for token in doc:
                    position = token.pos_
                    target_word = str(token)

                    if target_word in stopwords.words('english'):
                        continue

                    if position:
                        try:
                            synonym = self.word2vec.get_synonym(token)
                        except KeyError:
                            synset = token._.wordnet.synsets()
                            if not synset:
                                continue
                            synonym = [syn.lower() for syn in synset[0].lemma_names()]

                        for word in synonym:
                            if not word in self.synonyms:
                                self.synonyms[word] = {"synonym":[], "category":[], "pred_score":[]}

                            if category in self.synonyms[word]["category"]:
                                continue

                            self.synonyms[word]["synonym"].append(synonym)
                            self.synonyms[word]["category"].append(category)
                            self.synonyms[word]["pred_score"].append(score)

        with open("synonyms.json", 'w') as json_file: 
            json.dump(self.synonyms, json_file)
        
        logging.info("saving synonyms dictionary to synonyms.json")

    def generate(self, formula, count=1):
        results = []
        x = 0
        while x<count :
            sentence = ' '.join(self.generate_list(self.best_fitness, formula))
            if not self.validate_sentence(sentence):
                continue
            sentence = sentence[0].upper() + sentence[1:] + "."
            results.append(sentence)
            x += 1

        return results

    def generate_list(self, grammar, axiom):
        s = list()
        if axiom in grammar:
            expansion = random.choice(grammar[axiom])
            for token in expansion.split():
                s.extend(self.generate_list(grammar, token))
        else:
            keys = axiom.split()
            if len(keys)>1:
                for token in keys:
                    s.extend(self.generate_list(grammar, token))
            else:
                s.append(axiom)
        return s

    def add_field(self, field_name, values):
        self.best_fitness[field_name] = values

    def validate_sentence(self, sentence):
        doc = nlp(sentence)

        verb_found = False
        adj_found = False
        noun_found = False

        for token in doc:
            if token.pos_=='VERB':
                verb_found = True
            elif token.pos_=='ADJ':
                adj_found = True
            elif token.pos_=='NOUN':
                noun_found = True
            elif token.tag_ == 'NNP' and not noun_found:
                try:
                    noun_synonym = self.word2vec.get_synonym(token)
                except KeyError:
                    continue
                noun_found = True

        if (verb_found or adj_found) and noun_found:
            return True
        else:
            return False
