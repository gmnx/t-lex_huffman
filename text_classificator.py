from transformers import pipeline

classifier = pipeline("sentiment-analysis", model="autonlp_classification", return_all_scores=False)

def classify_text(text:str):
    outputs = classifier(text)
    return outputs[0]